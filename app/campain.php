<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class campain extends Model
{
    //

    protected $table = 'campains';

    protected $fillable = [
    	'code',
    	'departure',
    	'departureFlightNumber',
    	'returnhajj',
    	'returnFlightNumber'
    ];

    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }

    public function rfids()
    {
        return $this->hasManyThrough('App\Rfid', 'App\Pligrim', 'campain');
    }
}
