<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ble extends Model
{
    //
    protected $table = 'bles';

    protected $fillable = [
    	'name',
    	'campain'
    ];
}
