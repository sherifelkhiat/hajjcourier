<?php

return [
    'addPligrim' => 'Add Pligrim',
    'firstName' => 'First Name',
    'lastName' => 'Last Name',
    'nationality' => 'Nationality',
    'number' => 'Number',
    'address' => 'Address',
    'campain' => 'Campain',
    'endDate' => 'End Date',
    'birthDate' => 'Birth Date',
    'age' => 'Age'
];