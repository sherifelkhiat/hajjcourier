@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span>Full Name:</span>{{ $pligrim->firstName . " " . $pligrim->lastName  }}</div>
                <div class="card-header"><span>Nationality:</span>{{ $pligrim->nationality}}</div>
                <div class="card-header"><span>Number Of suits:</span>{{ $pligrim->number}}</div>
                <div class="card-header"><span>Address:</span>{{ $pligrim->address}}</div>

                <div class="card-header"><span>EXP:</span>{{ $pligrim->endDate}}</div>

                <div class="card-header">
                    <button class="btn btn-success col-md-10">Delivered</button><br><br>
                    <button class="btn btn-primary col-md-10">Out Of Delivery</button><br><br>
                    <button class="btn btn-primary col-md-10">In Transite</button><br><br>
                    <button class="btn btn-primary col-md-10">Assigned</button>
                </div>

                <div class="card-body">

                    <a href="{{ route('rfid.generate', [ 'pl' => $pligrim->id ]) }}" class="btn btn-primary">{{ __('Generate RFID') }}</a>
                </div>
                @if(isset($rfids))
                @foreach($rfids as $rfid)
                <div class="card">
                    <div class="card-body">
                    {{ $rfid->rfid }}
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
