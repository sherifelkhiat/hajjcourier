<?php

namespace App\Http\Controllers;

use App\Pligrim;
use App\campain;
use Illuminate\Http\Request;

class PligrimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // index
        $campain = new Campain();

        $campains = $campain->get();

        return view('pligrim.index', compact('campains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pligrim = new Pligrim();

        $pligrim->create($request->all());

        $pligrims = $pligrim->get();

        

        return view('pligrim.show', compact('pligrims'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pligrim  $pligrim
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $pligrim = new Pligrim();

        $pligrims = $pligrim->get();

        return view('pligrim.show', compact('pligrims'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pligrim  $pligrim
     * @return \Illuminate\Http\Response
     */
    public function edit(Pligrim $pligrim)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pligrim  $pligrim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pligrim $pligrim)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pligrim  $pligrim
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pligrim $pligrim)
    {
        //
    }

    public function search(Request $request)
    {

        $campain = new Pligrim();

        $firstName = $request->all()['search'];

        $pligrims = $campain->where('firstName', 'LIKE', '%' . $firstName . '%')->get();

        return view('pligrim.show', compact('pligrims'));
    }

    public function showOne(Pligrim $pligrim)
    {
        $rfids = $pligrim->rfids()->get();

        return view('pligrim.showone', compact('pligrim', 'rfids'));
    }
}
