@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Campain</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('campain.store') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Code') }}</label>

                            <div class="col-md-9">
                                <input id="Code" type="text" class="form-control{{ $errors->has('Code') ? ' is-invalid' : '' }}" name="code" value="{{ old('Code') }}" required autofocus>

                                @if ($errors->has('Code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6 form-group">
                                <label for="email" class="col-form-label text-md-right">{{ __('Depature') }}</label>
            
                                    <input id="email" type="text" class="form-control{{ $errors->has('departure') ? ' is-invalid' : '' }}" name="departure" value="{{ old('departure') }}" required>

                                    @if ($errors->has('departure'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('departure') }}</strong>
                                        </span>
                                    @endif
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="email" class="col-form-label text-md-right">{{ __('Departure Flight Number') }}</label>

                                    <input id="email" type="text" class="form-control{{ $errors->has('departureFlightNumber') ? ' is-invalid' : '' }}" name="departureFlightNumber" value="{{ old('email') }}" required>

                                    @if ($errors->has('departureFlightNumber'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('departureFlightNumber') }}</strong>
                                        </span>
                                    @endif
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 form-group">
                                <label for="email" class="col-form-label text-md-right">{{ __('Return') }}</label>
            
                                    <input id="email" type="text" class="form-control{{ $errors->has('return') ? ' is-invalid' : '' }}" name="return" value="{{ old('return') }}" required>

                                    @if ($errors->has('return'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('return') }}</strong>
                                        </span>
                                    @endif
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="email" class="col-form-label text-md-right">{{ __('Return Flight Number') }}</label>

                                    <input id="email" type="text" class="form-control{{ $errors->has('returnFlightNumber') ? ' is-invalid' : '' }}" name="returnFlightNumber" value="{{ old('returnFlightNumber') }}" required>

                                    @if ($errors->has('returnFlightNumber'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('returnFlightNumber') }}</strong>
                                        </span>
                                    @endif
                            </div>

                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Campaign') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection