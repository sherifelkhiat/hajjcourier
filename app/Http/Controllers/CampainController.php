<?php

namespace App\Http\Controllers;

use App\campain;
use Illuminate\Http\Request;

class CampainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // index
        return view('campain.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campain = new Campain();
        
        $campain->create($request->all());

        $campains = $campain->get();

        return view('campain.show', compact('campains'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\campain  $campain
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $campain = new Campain();

        $campains = $campain->get();

        return view('campain.show', compact('campains'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\campain  $campain
     * @return \Illuminate\Http\Response
     */
    public function edit(campain $campain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\campain  $campain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, campain $campain)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\campain  $campain
     * @return \Illuminate\Http\Response
     */
    public function destroy(campain $campain)
    {
        //
    }

    public function search(Request $request)
    {

        $campain = new Campain();

        $code = $request->all()['search'];

        $campains = $campain->where('code', 'LIKE', '%' . $code . '%')->get();

        return view('campain.show', compact('campains'));
    }

    public function showOne(campain $campain)
    {
        $hotels = $campain->hotels()->get();

        return view('campain.showone', compact('campain', 'hotels'));
    }
}
