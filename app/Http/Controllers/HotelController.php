<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\campain;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, campain $campain)
    {
        //
        $hotel = new Hotel();

        $hotel->campain_id = $campain->id;

        $hotel->city = $request->all()['city'];

        $hotel->hotelName = $request->all()['hotelName'];

        $hotel->checkIn = $request->all()['checkIn'];

        $hotel->checkOut = $request->all()['checkOut'];

        $hotel->supervisorName = $request->all()['supervisorName'];

        $hotel->supervisorMobile = $request->all()['supervisorMobile'];

        $hotel->save();

        $hotels = $campain->hotels()->get();

        return view('campain.showone', compact('hotels', 'campain'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        //
    }
}
