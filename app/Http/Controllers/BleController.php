<?php

namespace App\Http\Controllers;

use App\Ble;
use App\campain;
use Illuminate\Http\Request;

class BleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('ble.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ble = new Ble();

        $ble->create($request->all());

        return Back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ble  $ble
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $campains = campain::all();

        $bles = Ble::all();

        return view('ble.show', compact('campains', 'bles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ble  $ble
     * @return \Illuminate\Http\Response
     */
    public function edit(Ble $ble)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ble  $ble
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $ble_id = $request->all()['ble_id'];

        $ble = Ble::findOrFail($ble_id);

        $ble->campain_id = $request->all()['campain_id'];

        if($ble->save())
            return view('ble.success');

        return back();
    }

    public function showrfids(campain $campain)
    {
        $rfids = $campain->rfids()->get();

        $campains = campain::all();

        $bles = Ble::all();

        return view('ble.show', compact('campains', 'bles', 'rfids', 'campain'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ble  $ble
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ble $ble)
    {
        //
    }
}
