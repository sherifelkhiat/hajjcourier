@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
              <div class="card-body">

                    <form method="POST" action="{{ route('campain.search') }}" aria-label="{{ __('Register') }}">
                            @csrf
                    

                        <div class="input-group">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">{{ __('Search') }}</button>
                          </span>
                          <input type="text" class="form-control" name="search">
                        </div>

                </form>

              </div>
            </div>

        	 @foreach($campains as $cm)
            <div class="card">
			  <div class="card-body">
			    <h5 class="card-title"><span>Code:</span>  {{ $cm->code }}</h5>
			    <p class="card-text"><span>Departure:</span>{{ $cm->departure }}</p>
          <a href="{{route('campain.showOne', ['campain' => $cm->id ])}}" class="btn btn-primary">Show Campaign</a>
			  </div>
			</div>
			@endforeach

        </div>
        <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <a href="{{route('campain')}}" class="btn btn-primary">{{ __("Create Campaign") }}</a>
                </div>
            </div>
    </div>
</div>
@endsection
