<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('pligim', 'PligrimController@index')->name('pligrim');
Route::post('pligim/store', 'PligrimController@store')->name('pligrim.store');
Route::get('pligim/show', 'PligrimController@show')->name('pligrim.show');
Route::post('pligim/search', 'PligrimController@search')->name('pligrim.search');
Route::get('pligim/show/{pligrim}', 'PligrimController@showOne')->name('pligrim.showOne');
Route::get('campain', 'CampainController@index')->name('campain');
Route::post('campain/store', 'CampainController@store')->name('campain.store');
Route::get('campain/show', 'CampainController@show')->name('campain.show');
Route::post('campain/search', 'CampainController@search')->name('campain.search');
Route::get('campain/show/{campain}', 'CampainController@showOne')->name('campain.showOne');
Route::get('rfid/create/{pligrim}', 'RfidController@create')->name('rfid.generate');
Route::post('hotel/store/{campain}', 'HotelController@store')->name('hotel.store');
Route::get('ble/show', 'BleController@show')->name('ble.show');
Route::get('ble/show/{campain}', 'BleController@showrfids')->name('ble.showrfids');
Route::get('ble/create', 'BleController@create')->name('ble.create');
Route::post('ble/store', 'BleController@store')->name('ble.store');
Route::post('ble/update', 'BleController@update')->name('ble.update');
