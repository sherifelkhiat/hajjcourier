<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pligrim extends Model
{
    //
    protected $table = 'pligrims';
    
    protected $fillable = [
    	'firstName', 
    	'lastName', 
    	'nationality', 
    	'number',
    	 'endDate',
    	  'age', 
    	  'birthDate', 
    	  'address', 
    	  'campain'
    ];

    public function rfids()
    {
        return $this->hasMany('App\Rfid');
    }
    
}
