@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-body">

                <div class="dropdown">
                  <button class="btn btn-secondary dropdown-toggle col-md-12" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Choose The Campain
                  </button>
                  <div class="dropdown-menu col-md-12" aria-labelledby="dropdownMenuButton">
                    @foreach($campains as $campain)
                    <a class="dropdown-item" href="{{ route('ble.showrfids', ['campain' => $campain->id ]) }}">{{ $campain->code }}</a>
                    @endforeach
                  </div>
                </div>

                <form method="POST" action="{{ route('ble.update') }}" aria-label="{{ __('Register') }}">
                        @csrf

                @if(isset($rfids))
                @foreach($rfids as $rfid)
                <div class="card">
                    <div class="card-body">
                    <input type="checkbox" name="rfids[]">{{ $rfid->rfid }}
                    </div>
                </div>
                @endforeach
                @endif




                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Choose Ble') }}</label>

                    <select class="form-control col-md-4" name="campain">

                            @foreach($bles as $ble)
                                <option value="{{ $ble->id }}">{{ $ble->name }}</option>    

                            @endforeach

                    </select>

                </div>

            </form>

              </div>
            </div>

            <div class="col-md-6 offset-md-4">
                <a href="{{route('pligrim')}}" class="btn btn-primary">Transmit To Blsse</a>
            </div>

            <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <a href="#" id="scan" class="btn btn-primary">Scan</a>
                </div>
            </div>


        </div>

       
    </div>
</div>
@endsection
