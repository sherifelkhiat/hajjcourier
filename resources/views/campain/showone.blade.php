@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span>Campaign Code:</span>{{ $campain->code }}</div>

                @if(isset($hotels))
                @foreach($hotels as $hotel)
                <div class="card">
                    <div class="card-body">
                    <span>City : {{ $hotel->city }}</span>
                    <br>
                    <span>Check In : {{ $hotel->checkIn }}</span>
                    <br>
                    <span>Check Out : {{ $hotel->checkOut }}</span>
                    <br>
                    <span>Supervisor Name : {{ $hotel->supervisorName }}</span>
                    </div>
                </div>
                @endforeach
                @endif

                <div class="card">
                    <div class="card-body">
                            <form method="POST" action="{{ route('hotel.store', ['campaign' => $campain->id]) }}" aria-label="{{ __('Register') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label">{{ __('City') }}</label>

                                <div class="col-md-9">
                                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label">{{ __('Hotel Name') }}</label>

                                <div class="col-md-9">
                                    <input id="hotelName" type="text" class="form-control{{ $errors->has('hotelName') ? ' is-invalid' : '' }}" name="hotelName" value="{{ old('hotelName') }}" required autofocus>

                                    @if ($errors->has('hotelName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('hotelName') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6 form-group">
                                    <label for="email" class="col-form-label text-md-right">{{ __('Check In') }}</label>
                
                                        <input id="checkIn" type="text" class="form-control{{ $errors->has('checkIn') ? ' is-invalid' : '' }}" name="checkIn" value="{{ old('checkIn') }}" required>

                                        @if ($errors->has('checkIn'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('checkIn') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="col-md-6 form-group">
                                    <label for="email" class="col-form-label text-md-right">{{ __('Check Out') }}</label>

                                        <input id="checkOut" type="text" class="form-control{{ $errors->has('checkOut') ? ' is-invalid' : '' }}" name="checkOut" value="{{ old('checkOut') }}" required>

                                        @if ($errors->has('checkOut'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('checkOut') }}</strong>
                                            </span>
                                        @endif
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label">{{ __('Supervisor Name') }}</label>

                                <div class="col-md-9">
                                    <input id="supervisorName" type="text" class="form-control{{ $errors->has('supervisorName') ? ' is-invalid' : '' }}" name="supervisorName" value="{{ old('supervisorName') }}" required autofocus>

                                    @if ($errors->has('supervisorName'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('supervisorName') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label">{{ __('Supervisor Mobile') }}</label>

                                <div class="col-md-9">
                                    <input id="supervisorMobile" type="text" class="form-control{{ $errors->has('supervisorMobile') ? ' is-invalid' : '' }}" name="supervisorMobile" value="{{ old('supervisorMobile') }}" required autofocus>

                                    @if ($errors->has('supervisorMobile'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('supervisorMobile') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Create Hotel') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>
                

            </div>
        </div>
    </div>
</div>
@endsection
