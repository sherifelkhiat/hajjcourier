@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-body">

                    <form method="POST" action="{{ route('pligrim.search') }}" aria-label="{{ __('Register') }}">
                            @csrf
                    

                        <div class="input-group">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">{{ __('Search') }}</button>
                          </span>
                          <input type="text" class="form-control" name="search">
                        </div>

                </form>

              </div>
            </div>
        	
        	 @foreach($pligrims as $pl) 
            <div class="card">
			  <div class="card-body">
			    <h5 class="card-title"><span>First Name:</span>{{ $pl->firstName }}</h5>
			    <p class="card-text"><span>Nationality:</span>{{ $pl->nationality }}</p>
                <a href="{{route('pligrim.showOne', ['pligrim' => $pl->id ])}}" class="btn btn-primary">Show Pligrim</a>
			  </div>
			</div>
			@endforeach

			


        </div>
        <div class="form-group row">
            <div class="col-md-6 offset-md-4">
                <a href="{{route('pligrim')}}" class="btn btn-primary">Create Pligrim</a>
            </div>
        </div>
    </div>
</div>
@endsection
